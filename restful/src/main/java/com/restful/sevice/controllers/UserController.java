package com.restful.sevice.controllers;

import java.sql.Date;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

import javax.jws.soap.SOAPBinding.Use;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.domain.entities.User;
import com.entities.dao.daoUser;

@RestController
@RequestMapping("/User")
public class UserController {
	/*
	 * ------------------------------------For work with
	 * Maps--------------------------------------------- import java.util.Map;
	 * import java.util.stream.Collectors; import
	 * org.springframework.http.MediaType; Map<Integer, User>* Map<Integer, User>
	 * map = listUser.stream().collect(Collectors.toMap(User::getId, user -> user));
	 */

	@GetMapping(path = ("/Get/All"))
	public List<User> GetAll() throws Exception {
		List<User> listUser = null;
		try {
			listUser = daoUser.Instancia().GetAll();
		} catch (Exception e) {}
		return listUser;
	}

	@GetMapping(path = ("/Get/{id}"))
	public User Get(@PathVariable int id) throws Exception {
		User objUser = null;
		try {
			objUser = daoUser.Instancia().Get(id);
		} catch (Exception e) {}
		return objUser;

	}

	@PostMapping(path = ("/Update"))
	public ResponseEntity<User> Update(@RequestBody User objUser) throws Exception {
		try {
			daoUser.Instancia().Update(objUser);
		} catch (Exception e) {
			// TODO: handle exception
		}
		return new ResponseEntity<User>(objUser, HttpStatus.OK);
	}

	@PostMapping(path = ("/Insert"))
	public ResponseEntity<User> Insert(@RequestBody User objUser) throws Exception {
		try {
			daoUser.Instancia().Insert(objUser);
		} catch (SQLException e) {
    	    System.out.println("SQLException: " + e.getMessage());
		}
		return new ResponseEntity<User>(objUser, HttpStatus.OK);
	}
	
	@GetMapping(path = ("/Delete/{id}"))
	public Boolean Delete(@PathVariable int id) throws Exception {
		Boolean isDeleted = false;
		try {
			isDeleted = daoUser.Instancia().Delete(id);
		} catch (Exception e) {

		}
		return isDeleted;

	}
}
