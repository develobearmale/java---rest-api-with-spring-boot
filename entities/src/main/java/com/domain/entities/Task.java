package com.domain.entities;

import java.sql.Date;

public class Task {
	
	private int id;
	private int name;
	private Date create_date;
	private Date complete_date;
	private boolean complete;
	private User objUser;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getName() {
		return name;
	}
	public void setName(int name) {
		this.name = name;
	}
	public Date getCreate_date() {
		return create_date;
	}
	public void setCreate_date(Date create_date) {
		this.create_date = create_date;
	}
	public Date getComplete_date() {
		return complete_date;
	}
	public void setComplete_date(Date complete_date) {
		this.complete_date = complete_date;
	}
	public boolean isComplete() {
		return complete;
	}
	public void setComplete(boolean complete) {
		this.complete = complete;
	}
	public User getObjUser() {
		return objUser;
	}
	public void setObjUser(User objUser) {
		this.objUser = objUser;
	}
	public Task(int id, int name, Date create_date, Date complete_date, boolean complete, User objUser) {
		super();
		this.id = id;
		this.name = name;
		this.create_date = create_date;
		this.complete_date = complete_date;
		this.complete = complete;
		this.objUser = objUser;
	}
	
	

}
