package com.domain.entities;

import java.sql.Date;

public class User {
	
	private int id;
	private String username;
	private String password;
	private boolean active;
	private Date create_date;
	private Date update_date;
	private String email;
	
		
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public boolean isActive() {
		return active;
	}


	public void setActive(boolean active) {
		this.active = active;
	}

	public Date getCreate_date() {
		return create_date;
	}

	public void setCreate_date(Date create_date) {
		this.create_date = create_date;
	}

	public Date getUpdate_date() {
		return update_date;
	}

	public void setUpdate_date(Date update_date) {
		this.update_date = update_date;
	}
	
	

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public User(int id, String username, String password, boolean active, Date create_date, Date update_date, String email) {
		this.id = id;
		this.username = username;
		this.password = password;
		this.active = active;
		this.create_date = create_date;
		this.update_date = update_date;
		this.email = email;
	}
	
	public User() {}
	

}
