package com.util.dao;
import java.io.FileInputStream;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Properties;

public class Connexion {
	
    private static final Connexion Instance = new Connexion();
    

    //private constructor.
    private Connexion(){}
    
    private static String ChainConnection(String host,String port,String database, String user, String password) {
    	final String objConnection = ("jdbc:mysql://"+host+":"
        		+  port+"/"
        		+  database+"?"
                + "user="+user+"&"
                + "password="+password);
    	return objConnection;
    }

    public static Connection Connect() throws Exception {
    	Properties prop = new Properties();
    	InputStream input = null;
	    Connection connection = null;

    	try {
            input = new FileInputStream("db.properties");
            prop.load(input);

            Class.forName("com.mysql.cj.jdbc.Driver");
            connection = DriverManager
                    .getConnection(ChainConnection(prop.getProperty("host"),
                    							   prop.getProperty("port"),
                    							   prop.getProperty("database"),
                    							   prop.getProperty("user"),
                    							   prop.getProperty("password")));
    	}
    	catch (SQLException ex) {
    	    System.out.println("SQLException: " + ex.getMessage());
		}
    	
		return connection;

    }
   

}
