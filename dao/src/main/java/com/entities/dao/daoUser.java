package com.entities.dao;
import java.sql.Connection;
import java.sql.Date;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.domain.entities.User;
import com.mysql.cj.jdbc.CallableStatement;
import com.util.dao.Connexion;
import java.sql.ResultSet;
import java.sql.SQLException;

public class daoUser {
	
	public static daoUser _Instancia;
	private daoUser() {};

	public static daoUser Instancia() {
		if(_Instancia==null) {
			_Instancia = new daoUser();
		}
		return _Instancia;
	}
	
	public User Get(int id)throws Exception{
		final User objUser = new User();
		
		Connection cn = Connexion.Connect();
	    String query = "SELECT * FROM users WHERE id="+id;
	    Statement st = cn.createStatement();
	    ResultSet rs = st.executeQuery(query);
		
        while(rs.next()){
			objUser.setId(rs.getInt("id"));
			objUser.setUsername(rs.getString("username"));
			objUser.setEmail(rs.getString("email"));
			objUser.setPassword(rs.getString("password"));
	        objUser.setCreate_date(rs.getDate("created_at"));
	        objUser.setUpdate_date(rs.getDate("updated_at"));
	        objUser.setActive(rs.getBoolean("active"));
        }
        
		return objUser;
	}
	
	public List<User> GetAll() throws Exception{
		final List<User> listUser = new ArrayList<User>();
		Connection cn = Connexion.Connect();
	    String query = "SELECT * FROM users WHERE active=1";
	    Statement st = cn.createStatement();
	    ResultSet rs = st.executeQuery(query);
	    
	      while (rs.next())
	      {
			User objUser = new User();
		
			objUser.setId(rs.getInt("id"));
			objUser.setUsername(rs.getString("username"));
			objUser.setEmail(rs.getString("email"));
			objUser.setPassword(rs.getString("password"));
	        objUser.setCreate_date(rs.getDate("created_at"));
	        objUser.setUpdate_date(rs.getDate("updated_at"));
	        objUser.setActive(rs.getBoolean("active"));
	        
	        listUser.add(objUser);
	        
	      }
		
		return listUser;
	}
	
	public Boolean Update(User objUser)throws Exception{
		boolean isUpdated = false;
		
		Connection cn = Connexion.Connect();
	    String query = "{call UpdateUser(?,?,?)}";
	    CallableStatement stmt = (CallableStatement) cn.prepareCall(query);

	    try {
		    stmt.setInt(1, objUser.getId());
		    stmt.setString(2, objUser.getEmail());
		    stmt.setString(3, objUser.getPassword());
		    stmt.execute();
		    isUpdated=true;
	    }catch (Exception e) {
			// TODO: handle exception
		}

		return isUpdated;
	}
	
	public Boolean Insert(User objUser)throws Exception{
		boolean isCreated = false;

		Connection cn = Connexion.Connect();
	    String query = "{call InsertUser(?,?,?)}";
	    CallableStatement stmt = (CallableStatement) cn.prepareCall(query);

	    try {
		    stmt.setString(1, objUser.getUsername());
		    stmt.setString(2, objUser.getEmail());
		    stmt.setString(3, objUser.getPassword());
		    stmt.execute();
		    isCreated=true;
	    }catch (SQLException e) {
			// TODO: handle exception
		}
				
		return isCreated;
	}
	
	public Boolean Delete(int id)throws Exception{
		boolean isDeleted = false;
		
		Connection cn = Connexion.Connect();
	    String query = "{call DeleteUser(?)}";
	    CallableStatement stmt = (CallableStatement) cn.prepareCall(query);

	    try {
		    stmt.setInt(1, id);
		    stmt.execute();
		    isDeleted=true;
	    }catch (SQLException e) {
			// TODO: handle exception
		}

		return isDeleted;
	}
	
	
	
}
